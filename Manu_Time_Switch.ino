// Requies installation of USB drivers:
// http://www.prolific.com.tw/US/ShowProduct.aspx?p_id=229&pcid=41
// Requies installation of library: DS1307RTC by Michael Margollis

#include <DS1307RTC.h>
#include <Time.h>
#include <TimeLib.h>
#include <Wire.h>

#define TIME_MSG_LEN  11   // time sync to PC is HEADER followed by Unix time_t as ten ASCII digits
#define TIME_HEADER  'T'   // Header tag for serial time sync message
//#define TIME_REQUEST  7    // ASCII bell character requests a time sync message 

// Helpers for converting time
// Set RTC to computer time
// TZ_adjust=5.5;d=$(date +%s);t=$(echo "60*60*$TZ_adjust/1" | bc);echo T$(echo $d+$t | bc ) > /dev/tty.usbmodemfd121
// Set RTC to 25/10/2014 17:59:55
// TZ_adjust=5.5;d=1414240195;t=$(echo "60*60*$TZ_adjust/1" | bc);echo T$(echo $d+$t | bc ) > /dev/tty.usbmodemfd121
// Set RTC to 5/10/2014 21:29:55
// TZ_adjust=5.5;d=1414252795;t=$(echo "60*60*$TZ_adjust/1" | bc);echo T$(echo $d+$t | bc ) > /dev/tty.usbmodemfd121

tmElements_t tm;
tmElements_t RTCTime;
int RelayPin = 2;
int StatusPin = 13;
long CurrentSecond;
// Start lights at 18:00 add 'L' to make it Long
long StartSecond = 18 * 60 * 60L;
// Stop lights at 22:30 add 'L' to make it Long
long StopSecond = 22 * 60 * 60L + 30 * 60L;
int SleepInterval = 10000;
boolean debugEnabled = true;


void setup() {
  pinMode(RelayPin, OUTPUT);
  digitalWrite(RelayPin, LOW);
  
  Serial.begin(9600);
  while (!Serial) ; // wait for serial
    delay(200);

  Serial.println("");
  Serial.println("=========================================");
  Serial.println("= Surrendering to light v0.4 20/06/2017 =");
  Serial.println("=========================================");
  Serial.println("");
  Serial.println("To provide updated time via serial port:");
  Serial.println("TZ_adjust=5.5;d=$(date +%s);t=$(echo \"60*60*$TZ_adjust/1\" | bc);echo T$(echo $d+$t | bc ) > /dev/tty.ADD_USB_MODEM_DEV_NUMBER");
  Serial.println("");
}

void loop() {
  // Write RTC Time via Serial
  if(Serial.available() ) 
  {
    // Get data from Serial port
    // Decode
    // Write to Arduino Time
    processSyncMessage();

    // Write Arduino Time to RTC
    RTCTime.Hour = hour();
    RTCTime.Minute = minute();
    RTCTime.Second = second();
    RTCTime.Day = day();
    RTCTime.Month = month();
    RTCTime.Year = year() - 1970;
    RTC.set(makeTime(RTCTime));

    // Display set time
    Serial.println("\n==========");
    Serial.print("Updating time to: ");
    Serial.print(hour());
    Serial.write(':');
    printDigits(minute());
    Serial.write(':');  
    printDigits(second());
    Serial.print(" ");
    Serial.print(day());
    Serial.print("/");
    Serial.print(month());
    Serial.print("/");
    Serial.println(year()); 
    Serial.println("==========\n");
  }


  // Read time in RTC ...
  if(RTC.read(tm))
  {
    if(debugEnabled)  
    {
      Serial.print("RTC Time is: ");
      printDigits(tm.Hour);
      Serial.write(':');
      printDigits(tm.Minute);
      Serial.write(':');
      printDigits(tm.Second);
      Serial.print(" ");
      Serial.print(tm.Day);
      Serial.write('/');
      Serial.print(tm.Month);
      Serial.write('/');
      Serial.println(tmYearToCalendar(tm.Year));
    }

    // Add 'L' to make it Long
    CurrentSecond = tm.Hour * 60 * 60L + tm.Minute * 60L + tm.Second * 1L;
    
//    if(debugEnabled)  
//    {
//      Serial.print("StartSecond: ");
//      Serial.println(StartSecond);
//      Serial.print("StopSecond: ");
//      Serial.println(StopSecond);
//      Serial.print("CurrentSecond: ");
//      Serial.println(CurrentSecond);
//    }
    
    if(CurrentSecond > StartSecond && CurrentSecond < StopSecond)
    {
      digitalWrite(RelayPin, HIGH);
      digitalWrite(StatusPin, HIGH);
      if(debugEnabled)  
        Serial.println("Light ON !!");
    }
    else
    {
      digitalWrite(RelayPin, LOW);
      digitalWrite(StatusPin, LOW);
      if(debugEnabled)  
        Serial.println("Light OFF :(");
    }
  }
  // Something went wrong with getting time from DS1307
  else
  {
    if(debugEnabled)
    {
      if(RTC.chipPresent())
      {
        Serial.println("The DS1307 is stopped.  Please run the SetTime");
        Serial.println("example to initialize the time and begin running.");
        Serial.println();
      } 
      else {
        Serial.println("DS1307 read error!  Please check the circuitry.");
        Serial.println();
      }
    }
    delay(SleepInterval*10);
  }
  
  // Put board to sleep 
  delay(SleepInterval);
}

void printDigits(int digits){
  if(digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

// if time sync available from serial port, update time and return true
void processSyncMessage() {
  while(Serial.available() >=  TIME_MSG_LEN ){  // time message consists of header & 10 ASCII digits
    char c = Serial.read() ; 
    Serial.print(c);  
    if( c == TIME_HEADER ) {       
      time_t pctime = 0;
      for(int i=0; i < TIME_MSG_LEN -1; i++){   
        c = Serial.read();          
        if( c >= '0' && c <= '9'){   
          pctime = (10 * pctime) + (c - '0') ; // convert digits to a number    
        }
      }   
      setTime(pctime);   // Sync Arduino clock to the time received on the serial port
    }  
  }
}

